"""Constants loaded from evironment."""

from os import getenv
from typing import Optional

HOMECHAT_ID: int = int(getenv("HOME_CHAT") or 0)
assert HOMECHAT_ID, "Home chat ID is not provided"

BOT_TOKEN: Optional[str] = getenv("BOT_TOKEN")
assert BOT_TOKEN, "Bot token is not provided"
