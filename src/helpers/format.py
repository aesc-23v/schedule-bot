"""Text formatting utititary functions."""

from datetime import date, datetime, time
from typing import Optional

from helpers.data import get_lessons_info, get_lessons_time, get_schedule
from helpers.lessons import get_current_lesson


def bold(string: str) -> str:
    """Make text bold."""
    if not string:
        return ""
    return f"<b>{string}</b>"


def get_time_comment(deltas: list[int], now: datetime,
                     lesson_start: datetime) -> Optional[str]:
    """Get text saying that lesson will start soon.

    If there is no more lessons today, returns `None`
    """
    for delta in deltas:
        if (lesson_start - now).seconds // 60 == delta:
            if delta > 0:
                last_digit: int = delta % 10
                return f'''Через {delta} {
                    'минуту' if last_digit == 1
                    else 'минут' if last_digit in (0, 5, 6, 7, 8, 9)
                    else 'минуты'
                } начнётся'''

            return 'Сейчас начнётся'
    return None


def get_schedule_representation(weekday: int) -> str:
    """Get human-readable schedule representation for specified day."""
    current_lesson: Optional[int] = (get_current_lesson()
                                     if date.today().weekday() == weekday
                                     else None)

    return '\n'.join((bold(f'{s} — сейчас') if i == current_lesson else s)
                     for i, s in enumerate([
                         f'{i + 1}. {get_lesson_line(weekday, i)}'
                         for i in range(len(get_schedule()[weekday]))
                     ])) or bold("Завтра нет уроков")


def get_lesson_representation(weekday: int, lesson_index: int) -> str:
    """Get human-readable lesson representation."""
    lesson: str = get_schedule()[weekday][lesson_index]
    return f"{get_lesson_line(weekday, lesson_index)}"\
           f"\n\n{get_lessons_info()[lesson]}"


def get_lesson_line(weekday: int, lesson_index: int) -> str:
    """Get short lesson representation.

    Example: Physics (08:45 - 09:30)
    """
    lesson_times: list[tuple[time, time]] = get_lessons_time()
    today_schedule: list[str] = get_schedule()[weekday]

    if not today_schedule[lesson_index]:
        return ""

    return f"""{today_schedule[lesson_index]} ({
        ' - '.join(
            map(lambda t: t.isoformat('minutes'),
                lesson_times[lesson_index])
        )
    })"""
