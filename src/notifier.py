"""Notifier that sends notification about upcoming lessons before them.

Sends two notifications before every online lesson:
in 5 minutes and right before start.
"""

import asyncio
from datetime import datetime
from typing import Optional

from aiogram import Bot

from helpers.data import get_lessons_time, get_schedule, is_online
from helpers.env import HOMECHAT_ID
from helpers.format import bold, get_lesson_representation, get_time_comment
from helpers.lessons import get_next_lesson
from helpers.messages import send_message


async def notify_lesson(bot: Bot, deltas: list[int]):
    """Send notification if lesson will start soon."""
    next_lesson: Optional[int] = get_next_lesson()

    if next_lesson is None:
        return

    now: datetime = datetime.now()
    lesson_start: datetime = datetime.combine(
        date=now.date(), time=get_lessons_time()[next_lesson][0])

    time_comment: Optional[str] = get_time_comment(deltas, now, lesson_start)

    lesson_name: str = get_schedule()[now.weekday()][next_lesson]
    if not is_online(lesson_name):
        return

    if time_comment is None:
        return

    await send_message(
        bot, HOMECHAT_ID, f'{bold(time_comment + ":")}\n\n' +
        get_lesson_representation(now.weekday(), next_lesson))


async def run_notify_loop(bot: Bot):
    """Send notifications if needed every minute."""
    while True:
        await notify_lesson(bot, [5, 0])
        await asyncio.sleep(60)
