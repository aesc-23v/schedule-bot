"""Test getting lesson by time."""

from freezegun import freeze_time

from helpers.lessons import get_current_lesson, get_next_lesson


def test_current_lesson():
    """Test `helpers.lessons.get_current_lesson` function."""
    with freeze_time('Jan 12th, 2021, 12:30'):
        assert get_current_lesson() == 2

    with freeze_time('Jan 12th, 2021, 13:30'):
        assert get_current_lesson() is None

    with freeze_time('Jan 12th, 2021, 23:30'):
        assert get_current_lesson() is None

    with freeze_time('Nov 13th, 2021, 12:34'):
        assert get_current_lesson() is None


def test_next_lesson():
    """Test `helpers.lessons.get_next_lesson` function."""
    with freeze_time('Jan 12th, 2021, 12:30'):
        assert get_next_lesson() == 3

    with freeze_time('Jan 12th, 2021, 13:30'):
        assert get_next_lesson() == 3

    with freeze_time('Jan 12th, 2021, 23:30'):
        assert get_next_lesson() is None

    with freeze_time('Jan 12th, 2021, 18:30'):
        assert get_next_lesson() is None

    with freeze_time('Nov 13th, 2021, 10:15'):
        assert get_next_lesson() == 4
