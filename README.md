<h1 align="center">
AESC Schedule Bot
</h1>

<p align="center"><strong>Бот для расписания 10В СУНЦ МГУ</strong></p>

### Возможности

- Вывод расписания на текущий день
- Вывод расписания на следующий день
- Вывод информации о текущем уроке
- Вывод информации о следующем уроке
- Авторассылка перед уроком

### Использование

```bash
git clone https://gitlab.com/aesc-23v/schedule-bot
cd schedule-bot
pipenv install
export BOT_TOKEN="<токен_вашего_бота>"
pipenv run python src/main.py
```

или

```bash
git clone https://gitlab.com/aesc-23v/schedule-bot
cd schedule-bot
./run.sh "<токен_вашего_бота>"
```
