FROM python:3

WORKDIR /srv/bot

COPY Pipfile Pipfile.lock .

COPY LICENSE .

RUN pip install pipenv
RUN pipenv install --system --deploy

COPY src/ src/
COPY data/ data/

CMD python src/main.py
